# noinspection PyProtectedMember
from .ast import Statement, ValueOrLiteral, Signal
from typing import Iterable, Union, Any, Mapping

__all__ = ["Module"]


class _ModuleBuilderProxy:
    pass


Statements = Union[Statement,
                   Iterable[Statement]]


class _ModuleBuilderDomainExplicit(_ModuleBuilderProxy):
    def __iadd__(self,
                 assigns: Statements) -> '_ModuleBuilderDomainExplicit':
        ...


class _ModuleBuilderDomainImplicit(_ModuleBuilderProxy):
    sync: _ModuleBuilderDomainExplicit
    comb: _ModuleBuilderDomainExplicit


class _ModuleBuilderRoot:
    domain: _ModuleBuilderDomainImplicit
    d: _ModuleBuilderDomainImplicit


class _ValuelessContext:
    def __enter__(self) -> None:
        ...

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> bool:
        ...


class FSM:
    def __init__(self,
                 state: Signal,
                 encoding: Mapping[str, int],
                 decoding: Mapping[int, str]):
        self.state = state
        self.encoding = encoding
        self.decoding = decoding

    def ongoing(self, name: str) -> bool:
        ...


# noinspection PyPep8Naming
class Module(_ModuleBuilderRoot):
    submodules: Any

    def If(self, cond: ValueOrLiteral) -> _ValuelessContext:
        ...

    def Elif(self, cond: ValueOrLiteral) -> _ValuelessContext:
        ...

    def Else(self) -> _ValuelessContext:
        ...

    def Switch(self, test: ValueOrLiteral) -> _ValuelessContext:
        ...

    def Case(self, value: Union[None, int, bool] = None) -> _ValuelessContext:
        ...

    def elaborate(self, platform: Any) -> Any:
        ...
